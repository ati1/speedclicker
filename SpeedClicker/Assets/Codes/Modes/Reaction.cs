﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Reaction : GameMode
{
    public float ReactionTime { get { return m_reactionTime; } set { m_reactionTime = value; } }

    private float m_reactionTime = 0;
    private bool m_startReactionTimer = false;
    
    public override void Initialize()
    {
        base.Initialize();
    }

    public override void DoLogic()
    {
        base.DoLogic();

        if(m_startReactionTimer)
        {
            ReactionTime += Time.deltaTime;
        }

#if UNITY_STANDALONE
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Vector2 m = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                                     Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
#elif UNITY_ANDROID
        Vector2 m = TouchInputHelper.Get2DTouchPosition((Touch)TouchInputHelper.OnTouch());

        if(m != Vector2.zero)
        {
#endif
            RaycastHit2D h = Physics2D.Raycast(m, Vector2.zero);

            if (h.collider != null)
            {
                Circle c = h.collider.GetComponent<Circle>();

                if (c != null)
                {
                    StopReactionTimer();
                    c.ScoreAmount *= (int)(1000 / ReactionTime);
                    c.OnClick();
                    Victory();
                }
                else
                {
                    StaticValues.GameManager.GameOver();
                }
            }
            StaticValues.GameManager.GameOver();
        }

        StaticValues.GameManager.UpdateTimeText("RTime: " + ReactionTime.ToString("F1") + " s");
        StaticValues.GameManager.SpawnCirclesRandomWithFakeReaction(1,5);
    }

    public override void ResetGame(bool reload)
    {
        base.ResetGame(reload);
        ReactionTime = 0;
    }

    public void StartReactionTimer()
    {
        m_startReactionTimer = true;
    }

    public void StopReactionTimer()
    {
        m_startReactionTimer = false;
    }

    private void Victory()
    {
        GameManager.GameActive = false;
    }
}
