﻿using UnityEngine;
using System.Collections;
using System;

public abstract class GameMode
{
    public virtual void Initialize()
    {
        StaticValues.Score = 0;
        StaticValues.ElapsedTime = 0;
        StaticValues.GameManager.UpdateScores(0);
        GameManager.IsGameOver = false;
    }

    public virtual void DoLogic()
    {

    }

    public virtual void ResetGame(bool reload)
    {
        StaticValues.ElapsedTime = 0;
        StaticValues.Score = 0;
        GameManager.IsGameOver = false;
        if(reload)
            Application.LoadLevel(Application.loadedLevel);
    }
}
