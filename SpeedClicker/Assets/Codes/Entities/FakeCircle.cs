﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FakeCircle : Entity
{
    public float Lifetime { get { return m_lifeTime; } set { m_lifeTime = value; } }
    [SerializeField]
    private float m_lifeTime = 0;

    private Vector2 m_scaleVector = new Vector2();

    public bool Clicked { get; set; }

    //private SpriteRenderer m_sRenderer;

    //private Collider2D m_collider;

    protected override void Awake()
    {
        base.Awake();
        m_scaleVector = MyTransform.localScale;
        //m_sRenderer = GetComponent<SpriteRenderer>();
        //m_collider = GetComponent<Collider2D>();
    }

    protected override void FixedUpdate()
    {
        if (Lifetime <= 0)
        {
           Destroy(this.gameObject);
        }

        if (Clicked)
        {
            Scale(true);
        }
        else
        {
            Scale(false);
        }

        Lifetime -= Time.deltaTime;
        base.FixedUpdate();
    }

    public void OnClick()
    {
        if (!Clicked)
        {
            Clicked = true;
            MyTransform.position -= Vector3.back;
            StaticValues.GameManager.GameOver();
        }
    }

    private void Scale(bool increase)
    {
        float inc = increase ? Time.deltaTime * 15 : Time.deltaTime * 7;

        m_scaleVector = new Vector2(MyTransform.localScale.x + (increase ? inc : -inc)
            , MyTransform.localScale.y + (increase ? inc : -inc));
        MyTransform.localScale = m_scaleVector;
    }
}
