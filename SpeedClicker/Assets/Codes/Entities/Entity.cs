﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour 
{
	public bool IsActive{ get; set;}

	protected Transform MyTransform{ get; private set; }

	protected virtual void Awake()
	{
		MyTransform = this.transform;
	}

	protected virtual void FixedUpdate()
	{

	}
}