﻿using UnityEngine;
using System.Collections;

public class Circle : Entity 
{
	public float Lifetime{ get{ return m_lifeTime; }set{ m_lifeTime = value; } }
	[SerializeField]
	private float m_lifeTime = 0;

	public int ScoreAmount{ get{return m_scoreAmount;} set{m_scoreAmount = value;}}
	[SerializeField]
	private int m_scoreAmount = 0;

	private Vector2 m_scaleVector = new Vector2();

	public bool Clicked{ get; set;}

	private SpriteRenderer m_sRenderer;

    private Collider2D m_collider;
	
	protected override void Awake ()
	{
		base.Awake ();
		m_scaleVector = MyTransform.localScale;
		m_sRenderer = GetComponent<SpriteRenderer> ();
        m_collider = GetComponent<Collider2D>();
    }

	protected override void FixedUpdate ()
	{
		if(Lifetime <= 0)
		{
			if(!Clicked)
			{
				StaticValues.GameManager.GameOver();
			}

			Destroy(this.gameObject);
		}

        if(Clicked)
        {
            //MyTransform.Translate(-Camera.main.transform.forward * Time.deltaTime * 5);
            //Camera.main.transform.Translate(new Vector2(MyTransform.position.x,
            //   MyTransform.position.y) * Time.deltaTime * 0.015f);
            Scale(true);
        }
        else
        {
            Scale(false);
        }

		Lifetime -= Time.deltaTime;
		base.FixedUpdate ();
	}

	public void OnClick()
	{
		if(!Clicked)
		{
			Clicked = true;
			m_sRenderer.color = Color.green;
            StaticValues.GameManager.UpdateScores(m_scoreAmount);
            MyTransform.position -= Vector3.back;
            m_collider.enabled = false;
        }
		/*else
		{
			m_sRenderer.color = Color.red;
			StaticValues.GameManager.GameOver();
		}*/
	}

	private void Scale(bool increase)
	{
        float inc = increase ? Time.deltaTime * 15 : Time.deltaTime * 7;

		m_scaleVector = new Vector2( MyTransform.localScale.x + (increase ? inc : -inc)
            , MyTransform.localScale.y + (increase ? inc : -inc));
		MyTransform.localScale = m_scaleVector;
	}
}