﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameMode CurGameMode { get; set; }

	public static bool GameActive{ get; set;}

    public static bool IsGameOver{ get; set; }

	[SerializeField]
	private Collider2D m_gameField;

	[SerializeField]
	private Circle m_circlePrefab;

    [SerializeField]
    private FakeCircle m_fakeCPrefab;

	[SerializeField]
	private float m_spawnTimer;

	private float m_spawnTime;

	[SerializeField]
	private Text m_scoreText;

	[SerializeField]
	private Text m_timeText;

    [SerializeField]
    private Text m_startGameText;

    [SerializeField]
	private Text m_gameOverText;

	protected virtual void Awake()
	{
		StaticValues.GameManager = this;

        if (CurGameMode != null)
        {
            CurGameMode.Initialize();
        }
        else
        {
            CurGameMode = new Infinity();
            CurGameMode.Initialize();
        }

        m_startGameText.enabled = true;
	}

    private void UpdateTime()
    {
        StaticValues.ElapsedTime += Time.deltaTime;
    }

    public void UpdateTimeText(string text)
    {
        m_timeText.text = text;
    }

    public void UpdateScores(int amount)
    {
        StaticValues.Score += amount;
        if(m_scoreText != null)
            m_scoreText.text = "Score: " + StaticValues.Score.ToString();
    }
   
	protected virtual void Update()
	{
		if(GameActive)
		{
			UpdateTime();
			DoGameLogic();
		}
#if UNITY_STANDALONE
        else if (Input.GetKeyDown(KeyCode.Mouse0) && !IsGameOver)
#elif UNITY_ANDROID
        else if(TouchInputHelper.Touched() && !IsGameOver)
#endif
        {
            GameActive = true;
            m_startGameText.enabled = false;
        }
#if UNITY_STANDALONE
        else if (Input.GetKeyDown(KeyCode.Escape) && IsGameOver)
#elif UNITY_ANDROID
        else if(TouchInputHelper.Touched() && IsGameOver)
#endif
        {
			ResetGame(true);
		}
	}

	public void ResetGame(bool reload)
	{   
        CurGameMode.ResetGame(reload);
	}

    public void GoToMenu()
    {
        CurGameMode.ResetGame(false);
        Application.LoadLevel(0);
    }

	public void GameOver()
	{
		m_gameOverText.gameObject.SetActive(true);
		GameActive = false;
        IsGameOver = true;
	}

	protected void DoGameLogic()
	{
        CurGameMode.DoLogic();
	}

	public void SpawnCirclesRandom(float intensity)
	{
        if (m_spawnTime >= m_spawnTimer)
		{
			GameObject c = GameObject.Instantiate(m_circlePrefab.gameObject, GetRandomPosition(), Quaternion.identity) as GameObject;
            c.GetComponent<Circle>().ScoreAmount *= (int)intensity;
			m_spawnTime = 0;
		}
		m_spawnTime += Time.deltaTime * intensity;
	}

    public void SpawnCirclesRandomWithFake(float intensity, int ratio)
    {
        int random = Random.Range(0, ratio + 1);
        Entity e = (random < ratio) ? (Entity)m_circlePrefab : (Entity)m_fakeCPrefab;

        if (m_spawnTime >= m_spawnTimer)
        {
            GameObject c = GameObject.Instantiate(e.gameObject, GetRandomPosition(), Quaternion.identity) as GameObject;

            Circle circle = c.GetComponent<Circle>();
            if(circle != null)
                circle.ScoreAmount *= (int)intensity;

            m_spawnTime = 0;
        }
        m_spawnTime += Time.deltaTime * intensity;
    }

    public void SpawnCirclesRandomWithFakeReaction(float intensity, int circleRatio)
    {
        int random = Random.Range(0, circleRatio + 1);
        Entity e = (random < circleRatio) ? (Entity)m_fakeCPrefab : (Entity)m_circlePrefab;

        if (m_spawnTime >= m_spawnTimer)
        {
            GameObject c = GameObject.Instantiate(e.gameObject, GetRandomPosition(), Quaternion.identity) as GameObject;
            m_spawnTime = 0;

            Circle circle = c.GetComponent<Circle>();

            if(circle != null)
            {
                Reaction r = (Reaction)CurGameMode;
                r.StartReactionTimer();
            }
        }
        m_spawnTime += Time.deltaTime * intensity;
    }

    public Vector2 GetRandomPosition()
	{
		return new Vector2 (Random.Range (m_gameField.bounds.min.x, m_gameField.bounds.max.x),
		                    Random.Range (m_gameField.bounds.min.y, m_gameField.bounds.max.y));
	}
}