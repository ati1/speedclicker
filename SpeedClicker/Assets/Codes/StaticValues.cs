﻿using UnityEngine;
using System.Collections;

public static class StaticValues 
{
	public static GameManager GameManager{ get; set; }
	public static int Score{ get; set; }
	public static float ElapsedTime{ get; set; }
}

public enum GameModeEnum
{
    Infinity,
    Reaction
}