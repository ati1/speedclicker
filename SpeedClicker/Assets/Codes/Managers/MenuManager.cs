﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour
{


    public void StartGame(int gameMode)
    {
        switch((GameModeEnum)gameMode)
        {
            case GameModeEnum.Infinity:
                GameManager.CurGameMode = new Infinity();
                Application.LoadLevel(1);
                break;
            case GameModeEnum.Reaction:
                GameManager.CurGameMode = new Reaction();
                Application.LoadLevel(1);
                break;
        }
    }

    public void CloseApplication()
    {
        Application.Quit();
    }
}
