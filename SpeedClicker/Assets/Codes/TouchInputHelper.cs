﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class TouchInputHelper
{
    public static List<Touch> CurTouches
    {
        get
        {
            return Input.touches.ToList();
        }
    }

    public static Vector2 Get2DTouchPosition(Touch? t)
    {
        return (t == null) ? Vector2.zero : new Vector2(Camera.main.ScreenToWorldPoint(t.Value.position).x,
                                         Camera.main.ScreenToWorldPoint(t.Value.position).y);
    }

    public static Touch? OnTouch(Delegate d, object[] args)
    {
        if(Input.touchCount > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                if(d != null)
                    d.DynamicInvoke(args);
                return t;
            }
        }
        return null;
    }

    public static Touch? OnTouch()
    {
        if (Input.touchCount > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                return t;
            }
        }
        return null;
    }

    public static bool Touched()
    {
        if (Input.touchCount > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                return true;
            }
        }
        return false;
    }
}
