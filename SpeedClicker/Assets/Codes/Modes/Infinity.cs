﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Infinity : GameMode
{
    public Infinity()
    {
        Initialize();
    }

    public static float Intensity { get; set; }

    public override void Initialize()
    {
        base.Initialize();
        Intensity = 1;
        //GameManager.GameActive = true;
    }

    public override void ResetGame(bool reload)
    {
        base.ResetGame(reload);
        Intensity = 1;
    }

    public override void DoLogic()
    {
        base.DoLogic();
#if UNITY_STANDALONE
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Vector2 m = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                                     Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
#elif UNITY_ANDROID
        Vector2 m = TouchInputHelper.Get2DTouchPosition(TouchInputHelper.OnTouch());

        if(m != Vector2.zero)
        {
#endif
            RaycastHit2D h = Physics2D.Raycast(m, Vector2.zero);

            if (h.collider != null)
            {
                Entity e = h.collider.GetComponent<Entity>();

                //This is a really stupid way of checking which type the entity is
                if (e != null)
                {
                    try
                    {
                        Circle c = (Circle)e;
                        if (c != null)
                        {
                            c.OnClick();
                        }
                    }
                    catch
                    {
                        FakeCircle fC = (FakeCircle)e;
                        if (fC != null)
                        {
                            fC.OnClick();
                        }
                    }    
                }
                else
                {
                    StaticValues.GameManager.GameOver();
                }
            }
        }

        StaticValues.GameManager.UpdateTimeText("Time: " + StaticValues.ElapsedTime.ToString("F1") + " s");
        StaticValues.GameManager.SpawnCirclesRandomWithFake(Intensity, 4);
        IncreaseIntensity();
    }

    protected void IncreaseIntensity()
    {
        Intensity += Time.deltaTime / 15;
    }
}
